//
//  ViewController.swift
//  SimpleClock
//
//  Created by Iwaya Akira on 2017/05/17.
//  Copyright © 2017年 Akira Iwaya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var hour: UIImageView!
    
    @IBOutlet weak var minute: UIImageView!
    
    @IBOutlet weak var second: UIImageView!
    
    let calendar = Calendar(identifier: .gregorian)
    
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hour.layer.allowsEdgeAntialiasing = true
        minute.layer.allowsEdgeAntialiasing = true
        second.layer.allowsEdgeAntialiasing = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(1.0), repeats: true, block: { _ in
            
            let date = Date()
            let h = self.calendar.component(.hour, from: date)
            let m = self.calendar.component(.minute, from: date)
            let s = self.calendar.component(.second, from: date)
            
            print("\(h) \(m) \(s)")
            
            let pi2 = CGFloat.pi * 2
            
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.0, options: [], animations: {
                
                // 長針
                let minuteRotation = pi2 * (CGFloat(m) / 60.0)
                self.minute.transform = CGAffineTransform.identity.rotated(by: minuteRotation)
                
                // 秒針
                let secondRotation = pi2 * (CGFloat(s) / 60.0)
                self.second.transform = CGAffineTransform.identity.rotated(by: secondRotation)
                
            }, completion: nil)
            
            // 短針
            let totalSeconds = (h % 12) * 3600 + m * 60 + s // 0 ~ 43199
            let hourRotation = pi2 * CGFloat(totalSeconds) / CGFloat(12 * 60 * 60)
            self.hour.transform = CGAffineTransform.identity.rotated(by: hourRotation)
        })
        
        timer?.fire()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
    
}

